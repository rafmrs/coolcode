<?php

/**
 * Require composer packages
 */

require_once 'vendor/autoload.php';

/**
 * Classes need for bootstrap
 */

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/**
 * Router instanciator
 */

$container = new Container;
$request = Request::capture();
$container->instance('Illuminate\Http\Request', $request);
$events = new Dispatcher($container);
$router = new Router($events, $container);

/**
 * Errors Display
 */

if(App::get('debug') == 1){
	$whoops = new \Whoops\Run;
	$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
	$whoops->register();
}

/**
 * Boot database
 */

App::database();

/**
 * Require routes file
 */

require_once 'src/routes.php';

/**
 * Add routes to config file
 */

App::set('routes', $router->getRoutes());

/**
 * Set default timezone
 */

date_default_timezone_set(App::get('timezone'));

/**
 * Run application
 */

$request = Request::capture();
$response = $router->dispatch($request);
$response->send();