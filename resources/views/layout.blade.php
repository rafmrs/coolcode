<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="author" content="Rafael Caniçal">

	<link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
	<link rel="manifest" href="img/favicons/site.webmanifest">
	<link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#000000">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#ffffff">

    <meta property="og:title" content="Coolcode" />
    <meta property="og:image" content="{{ App::get('url') }}/img/share-img.jpg" />
    <meta property="og:description" content="It's like Pastebin but lightweight and with a shortcut for Carbon(carbon.now.sh)." />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ App::get('url') }}" />
    <meta property="og:site_name" content="Coolcode" />
    
    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="{{ App::get('url') }}">
    <meta name="twitter:title" content="Coolcode">
    <meta name="twitter:description" content="It's like Pastebin but lightweight and with a shortcut for Carbon(carbon.now.sh).">
    <meta name="twitter:image" content="{{ App::get('url') }}/img/share-img.jpg">

	<title>coolcode</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/tomorrow.min.css">
	<link rel="stylesheet" href="{{ App::get('url') }}/css/style.css">
</head>
<body>
	<div class="container">
        <a href="{{ App::get('url') }}" class="logo">
            <img width="30px" src="img/logo.svg">
            <span>Coolcode</span>
        </a>
        <ul>
            <li><a href="https://bitbucket.org/rafaelcanical/coolcode" target="_blank">Source</a></li>
            <li><a href="http://rafael.gq" target="_blank">Developer</a></li>
        </ul>

        @yield('content')
    </div>

    <footer>
        <div class="container">
            <div class="copy">
                All rights reserved &copy; {{ date('Y') }}
            </div>
            <ul>
                <li><a href="https://bitbucket.org/rafaelcanical/coolcode" target="_blank">Source</a></li>
                <li><a href="http://rafael.gq" target="_blank">Developer</a></li>
            </ul>
        </div>
    </footer>

    @yield('scripts')
</body>
</html>