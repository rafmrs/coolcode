@extends('layout')

@section('content')
	<h1>Save code and generate Carbon code images</h1>
	<textarea id="code" placeholder="Write your code here..."></textarea>
	<button id="carbon" onClick="goToCarbon()">Generate Carbon Image</button>
	<button id="create" onClick="createLink()">Create Link</button>
@endsection

@section('scripts')
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<script type="text/javascript">
		/**
		 * Generate link and go to Carbon
		 */
		function goToCarbon() {
			var code = encodeURIComponent(document.getElementById("code").value);
			var url = 'https://carbon.now.sh/?bg=rgba(255,255,255,1)&t=material&wt=none&l=jsx&ds=false&dsyoff=20px&dsblur=68px&wc=false&wa=false&pv=0px&ph=0px&ln=true&fm=Hack&fs=14px&lh=133%25&si=false&code=' + code + '&es=2x&wm=false&ts=false';
			var win = window.open(url, '_blank');
			win.focus();
		}

		/**
		 * Create a new link in the database
		 */
		function createLink() {
			var action = "{{ App::get('url') }}/new",
				code = document.getElementById("code").value;
			
			axios.post(action, {code: code}).then(function (response) {
				if(typeof response.data.objective != 'undefined'){
					if(response.data.objective){
						window.location.href = response.data.redirect;
					}else{
						alert(response.data.message);
					}
				}
			});
		}
	</script>
@endsection