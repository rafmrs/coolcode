@extends('layout')

@section('content')
	@if($code)
		<pre><code>{{ $code->code }}</code></pre>
		<textarea id="hidden-code">{{ $code->code }}</textarea>
		<button id="carbon" onClick="goToCarbon()">Generate Carbon Image</button>
		<button id="create" onClick="copyToClipboard()">Copy to Clipboard</button>
	@else
		<h1>404<br>Oops, code not found</h1>
	@endif
@endsection

@section('scripts')
	<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>

	<script type="text/javascript">
		hljs.initHighlightingOnLoad();

		/**
		 * Generate link and go to Carbon
		 */
		function goToCarbon() {
			var code = encodeURIComponent(document.getElementById("hidden-code").value);
			var url = 'https://carbon.now.sh/?bg=rgba(255,255,255,1)&t=material&wt=none&l=jsx&ds=false&dsyoff=20px&dsblur=68px&wc=false&wa=false&pv=0px&ph=0px&ln=true&fm=Hack&fs=14px&lh=133%25&si=false&code=' + code + '&es=2x&wm=false&ts=false';
			var win = window.open(url, '_blank');
			win.focus();
		}

		/**
		 * Copy the text to the clipboard
		 */
		function copyToClipboard() {
			var text = document.getElementById("hidden-code");
			text.select();
			document.execCommand("copy");
			alert("Code copied!");
		}
	</script>
@endsection