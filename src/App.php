<?php

use Illuminate\Config\Repository;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;

class App{

	/**
	 *
	 * Config static function
	 *
	 */
	public static $config = '';

	/**
	 *
	 * Initialize configuration object
	 *
	 */
	static function init()
	{
		if(self::$config == ''){
			self::$config = new Repository(require ROOT_PATH . '/src/config.php');
		}
	}

	/**
	 *
	 * Get a property of the configurations file
	 * 
	 * @param string $param Key name to get
	 * @return variable value 
	 */
	static function get($param = '')
	{
		self::init();
		return self::$config[$param];
	}

	/**
	 *
	 * Set a new property of the configurations file
	 *
	 * @param string $param New key name
	 * @param $value Value to inser on the newly created array
	 * @return boolean
	 *
	 */
	static function set($param = '', $value = '')
	{
		self::init();
		return self::$config->set($param, $value);
	}

	/**
	 *
	 * Configure and boot database
	 *
	 * @param boolean $boot If it will boot or not
	 *
	 */
	static function database($boot = true)
	{
		if($boot){
			$capsule = new Capsule;
			$capsule->addConnection([
				'driver'    => App::get('db.driver'),
				'host'      => App::get('db.host'),
				'database'  => App::get('db.name'),
				'username'  => App::get('db.user'),
				'password'  => App::get('db.pass'),
				'charset'   => App::get('db.charset'),
				'collation' => App::get('db.collation'),
				'prefix'    => App::get('db.prefix'),
			]);
			
			$capsule->setAsGlobal();
			$capsule->bootEloquent();
		}
	}

	/**
	 *
	 * Redirect initiation
	 *
	 * @return Redirector $redirect Redirector class instance
	 *
	 */
	static function redirect()
	{
		$request = Request::capture();
		$redirect = new Redirector(new UrlGenerator(self::get('routes'), $request));

		return $redirect;
	}
}