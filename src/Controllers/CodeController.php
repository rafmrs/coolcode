<?php

namespace Controllers;

use App;
use Models\Code;
use Core\Controller;
use Illuminate\Http\Request;

class CodeController extends Controller
{
	/**
	 * Index function with new code view
	 */
	public function new()
	{
		echo $this->view->render('new_code');
	}

	/**
	 * Index function with new code view
	 */
	public function createNew(Request $request)
	{
		$code = $request->post('code');
		
		if($code){
			$code = Code::create([
				'uid' => uniqid(),
				'code' => $code,
				'created_at' => date('Y-m-d H:i:s')
			]);
	
			$url = App::get('url') . '/' . $code->uid;
	
			echo json_encode(['objective' => true, 'redirect' => $url]);
		}else{
			echo json_encode(['objective' => false, 'message' => 'Please insert some code to create the link']);
		}
	}

	/**
	 * Show code, copy to clipboard and close window
	 */
	public function clip($uid)
	{
		$code = Code::where('uid', $uid)->first();

		$data = [
			'code' => $code
		];

		echo $this->view->render('clip_code', $data);
	}

	/**
	 * Show code
	 */
	public function readCode($uid)
	{
		$code = Code::where('uid', $uid)->first();

		$data = [
			'code' => $code
		];

		echo $this->view->render('show_code', $data);
	}

}