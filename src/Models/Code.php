<?php

namespace Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Code extends Eloquent
{
	public $timestamps = false;
	protected $table = 'codes';
	protected $fillable = ['uid', 'code', 'created_at'];
}