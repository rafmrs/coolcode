<?php

return [
    'debug' => 1,
    'timezone' => 'Europe/Lisbon',
    'url' => 'http://cc.rafael.gq',

    /**
     * Database settings
     */
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'name' => 'coolcode',
        'user' => 'root',
        'pass' => 'root',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => ''
    ]
];