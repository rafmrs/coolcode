<?php

use Illuminate\Routing\Router;

$router->group(['namespace' => 'Controllers'], function(Router $router){
	$router->get('/', ['uses' => 'CodeController@new']);
	$router->post('/new', ['uses' => 'CodeController@createNew']);
	$router->get('/clip/{uid}', ['uses' => 'CodeController@clip']);
	$router->get('/{uid}', ['uses' => 'CodeController@readCode']);
});