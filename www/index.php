<?php

/**
 * Define directory constants
 */
define('PUBLIC_PATH', __DIR__);
define('ROOT_PATH', dirname(__DIR__));

/**
 * Include bootstrap
 */
include(ROOT_PATH . '/bootstrap.php');